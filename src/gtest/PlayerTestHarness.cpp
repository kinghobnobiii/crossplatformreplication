#include <limits.h>
#include <math.h>
#include "gtest/gtest.h"

#include "PlayerTestHarness.h"
#include "Player.h"
#include "PlayerClient.h"
#include "TextureManager.h"
#include "Maths.h"
#include "Colors.h"
#include "NPC.h"

#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"

#include <iostream>
#include <fstream>
#include <thread>

/* Reference: http://www.yolinux.com/TUTORIALS/Cpp-GoogleTest.html */

PlayerTestHarness::PlayerTestHarness()
{
  pp = nullptr;
  np = nullptr;
}

PlayerTestHarness::~PlayerTestHarness()
{
  pp.reset();
  np.reset();
}

void PlayerTestHarness::SetUp()
{
    GameObject*	go = Player::StaticCreate();
    Player* p = static_cast<Player*>(go);
    this->pp.reset(p);

    GameObject* stop = NPC::StaticCreate();
    NPC* n = static_cast<NPC*>(stop);
    this->np.reset(n);
}

void PlayerTestHarness::TearDown()
{
    this->pp.reset();
    this->pp = nullptr;

    this->np.reset();
    this->np = nullptr;
}

TEST_F(PlayerTestHarness,constructor_noArgs)
{
  //Player Test
  EXPECT_TRUE(Maths::Is3DVectorEqual(pp->GetColor(), Colors::White));
  EXPECT_TRUE(Maths::Is3DVectorEqual(pp->GetLocation(), Vector3::Zero));
  EXPECT_FLOAT_EQ(pp->GetCollisionRadius(), 0.5f);
  EXPECT_FLOAT_EQ(pp->GetScale(),1.0f);
  EXPECT_FLOAT_EQ(pp->GetRotation(),0.0f);
  EXPECT_EQ(pp->GetIndexInWorld(), -1);
  EXPECT_EQ(pp->GetNetworkId(), 0);

  //NPC Test
  EXPECT_TRUE(Maths::Is3DVectorEqual(pp->GetColor(), Colors::White));
  EXPECT_TRUE(Maths::Is3DVectorEqual(pp->GetLocation(), Vector3::Zero));
  EXPECT_FLOAT_EQ(np->GetCollisionRadius(), 0.5f);
  EXPECT_FLOAT_EQ(np->GetScale(), 1.0f);
  EXPECT_FLOAT_EQ(np->GetRotation(), 0.0f);
  EXPECT_EQ(pp->GetIndexInWorld(), -1);
  EXPECT_EQ(pp->GetNetworkId(), 0);

  //Player Test
  EXPECT_TRUE(Maths::Is3DVectorEqual(pp->GetVelocity(), Vector3::Zero));
  EXPECT_EQ(pp->GetPlayerId(), 0.0f);

  //NPC Test
  EXPECT_TRUE(Maths::Is3DVectorEqual(np->GetVelocity(), Vector3::Zero));
  EXPECT_EQ(np->GetNPCId(), 0.0f);

  //Initial state is update all
  int check = 0x000F; //Hex - binary 00000000 00000000 00000000 00001111
  EXPECT_EQ(pp->GetAllStateMask(), check);

  //Player Test
  EXPECT_EQ(pp->GetClassId(), 'PLYR');
  EXPECT_NE(pp->GetClassId(), 'HELP');

  //NPC Test
  EXPECT_EQ(np->GetClassId(), 'NPCS');
  EXPECT_NE(np->GetClassId(), 'HELP');

  //Player Test
  EXPECT_FLOAT_EQ(pp->GetMaxLinearSpeed(),  50.0f);
  EXPECT_FLOAT_EQ(pp->GetMaxRotationSpeed(), 5.0f);
  EXPECT_FLOAT_EQ(pp->GetWallRestitution(), 0.1f);
  EXPECT_FLOAT_EQ(pp->GetNPCRestitution(), 0.1f);
  EXPECT_FLOAT_EQ(pp->GetLastMoveTimestamp(), 0.0f);
  EXPECT_FLOAT_EQ(pp->GetThrustDir(), 0.0f);
  EXPECT_EQ(pp->GetHealth(), 10);
  EXPECT_FALSE(pp->IsShooting());

  //NPC Test
  EXPECT_FLOAT_EQ(np->GetMaxLinearSpeed(), 50.0f);
  EXPECT_FLOAT_EQ(np->GetMaxRotationSpeed(), 5.0f);
  EXPECT_FLOAT_EQ(np->GetWallRestitution(), 0.1f);
  EXPECT_FLOAT_EQ(np->GetNPCRestitution(), 0.1f);
  EXPECT_FLOAT_EQ(np->GetLastMoveTimestamp(), 0.0f);
  EXPECT_FLOAT_EQ(np->GetThrustDir(), 0.0f);
  EXPECT_EQ(np->GetHealth(), 10);
  EXPECT_FALSE(np->IsShooting());
}


/* Tests Omitted
* There's a good chunk of this which cannot be tested in this limited example,
* however there should be enough to underake some testing of the serialisation code.
*/

TEST_F(PlayerTestHarness,EqualsOperator1)
{ /* Won't compile - why?
  Player a ();
  Player b ();

  a.SetPlayerId(10);
  b.SetPlayerId(10);

  EXPECT_TRUE(a == b);*/
}

TEST_F(PlayerTestHarness,EqualsOperator2)
{
  Player *a = static_cast<Player*>(Player::StaticCreate());
  Player *b = static_cast<Player*>(Player::StaticCreate());

  a->SetPlayerId(10);
  b->SetPlayerId(10);

  EXPECT_TRUE(*a == *b);
}

/* Need more tests here */

TEST_F(PlayerTestHarness,EqualsOperator3)
{
  Player *a = static_cast<Player*>(Player::StaticCreate());
  Player *b = static_cast<Player*>(Player::StaticCreate());

  a->SetPlayerId(10);
  b->SetPlayerId(30);

  EXPECT_FALSE(*a == *b);
}

TEST_F(PlayerTestHarness,EqualsOperator4)
{
  PlayerPtr b(static_cast<Player*>(Player::StaticCreate()));

  pp->SetPlayerId(10);
  b->SetPlayerId(10);

  EXPECT_TRUE(*pp == *b);
}

/* Serialistion tests in MemoryBitStreamTestHarness */